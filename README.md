# eslint-plugin-camper

Tools for fools

## Installation

You'll first need to install [ESLint](http://eslint.org):

```
$ npm i eslint --save-dev
```

Next, install `eslint-plugin-camper`:

```
$ npm install eslint-plugin-camper --save-dev
```

**Note:** If you installed ESLint globally (using the `-g` flag) then you must also install `eslint-plugin-camper` globally.

## Usage

Add `camper` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
    "plugins": [
        "camper"
    ]
}
```


Then configure the rules you want to use under the rules section.

```json
{
    "rules": {
        "camper/rule-name": 2
    }
}
```

## Supported Rules

* Fill in provided rules here





