/**
 * @fileoverview Prefer safe property-accessor functions instead of literal dot notation
 * @author brekk
 */
'use strict';
// const { reduce, pipe, curry, map } = require('ramda');
// const { trace } = require('xtrace');
const {
  fromSchema,
  safePathOr
  // safePathEq
  // safePathNotEq,
  // safePathOr
} = require('../utils');
const { preferNamedSelector } = require('../errors');
const {
  getLiteral,
  getLiteralWithThis,
  assertInAnd,
  assertNonFunction,
  assertParentIsIdentifier,
  assertParentIsMemberExpression,
  assertPropsX,
  assertThisProps,
  assertThisPropsX,
  assertThisPropsXIsLiteral,
  assertTypeIsMemberExpression,
  assertXIsLiteral,
  getGPaPropertyName,
  getOldestAncestor,
  getAllAncestors,
  anyOf
} = require('../selectors');

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = {
  meta: {
    docs: {
      description:
        'Prefer safe property-accessor functions instead of literal dot notation',
      category: 'safety',
      recommended: true
    },
    fixable: 'code',
    /*
     "use-selector-for-nested-property": ["error", {
       "es6": true,
       "literal": "stateData",
       "library": "ramda",
       "selector": "pathOr",
       "checkProps" false
     }]
     // using something like pathOr instead
     "use-selector-for-nested-property": ["error", {
       "selector": "pathOr",
       "defaultExpression": "const UNKNOWN = '???'",
       "defaultExpression": "import {UNKNOWN} from './constants'",
       "defaultName": "UNKNOWN",
     }]
    */
    schema: [
      {
        properties: {
          literal: {
            type: ['string', 'array'],
            items: { type: 'string' },
            description:
              'The name of the object literal. (default: "stateData")'
          },
          library: {
            type: 'string',
            description: 'The name of the accessor library (default: "ramda")'
          },
          selector: {
            type: 'string',
            description: 'The function name for selecting (default: "pathOr")'
          },
          defaultExpression: {
            type: 'string',
            description: 'A constant / reference to a re-usable default value'
          },
          defaultName: {
            type: 'string',
            description: 'a default value to pass to selector (default: false)'
          },
          checkProps: {
            type: 'boolean',
            description:
              'Check this.props.stateData | props.stateData as well as stateData.'
          }
        }
      }
    ]
  },

  create: function create(context) {
    const getLiteralName = fromSchema('stateData', 'literal');
    const getLibraryName = fromSchema('"ramda"', 'library');
    const getSelectorFunction = fromSchema('pathOr', 'selector');
    const getDefaultExpression = fromSchema(
      // TODO: figure out the best way to var / let / const
      null,
      'defaultExpression'
    );
    const getSelectorDefault = fromSchema(null, 'defaultName');
    const getCheckProps = fromSchema(false, 'checkProps');
    const reorderAccessor = ({ prefix, selector, def, literalName, node }) => {
      const top = getOldestAncestor(assertTypeIsMemberExpression, node);
      const x = getAllAncestors(assertTypeIsMemberExpression, node);
      const lastAncestorName = top && top.property && top.property.name;
      const firstAncestorName = getGPaPropertyName(node);
      const start = safePathOr(
        node.start,
        ['parent', 'object', 'property', 'parent', 'object', 'start'],
        node
      );
      const ancestors = x
        .slice(1)
        .map(z => z.name || (z.property && z.property.name));

      const allAncestors = [firstAncestorName]
        .concat(ancestors)
        .concat(lastAncestorName);

      const peeps = !prefix
        ? allAncestors
        : [...prefix.slice(1), literalName, ...allAncestors];
      if (prefix) {
        literalName = prefix[0];
      }
      const defValue = def ? `${def}, ` : ``;
      return {
        range: [start, top.property.end],
        fixed: `${selector}(${defValue}["${peeps.join(
          '", "'
        )}"], ${literalName})`
      };
    };
    const fixable = ({
      def,
      exp,
      lib,
      literalName,
      node,
      prefix,
      selector
    }) => fixer => {
      if (
        assertParentIsMemberExpression(node) &&
        assertParentIsIdentifier(node)
      ) {
        const { range, fixed } = reorderAccessor({
          prefix,
          selector,
          def,
          literalName,
          node
        });
        // console.log('fixing...', fixed, range);
        const importStatement = `import { ${selector} } from "${lib}"\n${
          def && exp ? exp : ''
        }\n`;
        return [
          fixer.insertTextBeforeRange([0, 1], importStatement),
          fixer.replaceTextRange(range, fixed)
        ];
      }
    };
    return {
      Identifier(node) {
        const literalName = getLiteralName(context);
        const isLiteralAList = Array.isArray(literalName);
        const isLiteralStar = literalName === '*';
        const lib = getLibraryName(context);
        const selector = getSelectorFunction(context);
        const def = getSelectorDefault(context);
        const exp = getDefaultExpression(context);
        const checkProps = getCheckProps(context);
        const fail = fix => {
          const message = preferNamedSelector(
            isLiteralAList ? literalName.join('|') : literalName
          );
          const { loc } = node;
          context.report(
            fix ? { node, message, loc, fix } : { node, message, loc }
          );
        };
        if (assertNonFunction(node)) {
          if (isLiteralStar) {
            // *.a.b
            if (assertXIsLiteral(() => true, node)) {
              return fail(
                fixable({
                  def,
                  exp,
                  lib,
                  literalName: getLiteral(node),
                  node,
                  prefix: false,
                  selector
                })
              );
            }
            if (checkProps) {
              // props.*.a
              if (assertPropsX(node)) {
                console.log('PROPS', node.name);
                return fail();
              }
              // this.props.*.a
              if (assertThisProps(node) && assertThisPropsX(node)) {
                return fail(
                  fixable({
                    def,
                    exp,
                    lib,
                    literalName: getLiteralWithThis(node),
                    node,
                    prefix: ['this', 'props'],
                    selector
                  })
                );
              }
            }
          }
          // for now, split on list, later, rectify
          if (isLiteralAList) {
            const assertXIsAnyLiteral = anyOf(assertXIsLiteral);
            const assertThisPropsXIsAnyLiteral = anyOf(
              assertThisPropsXIsLiteral
            );
            // (stateData|xxx).a.b
            if (assertXIsAnyLiteral(literalName, node)) {
              return fail(
                fixable({
                  def,
                  exp,
                  literalName: getLiteral(node),
                  node,
                  prefix: false,
                  selector
                })
              );
            }
            if (checkProps) {
              // props.(stateData|xxx).a.b
              if (assertPropsX(node)) {
                return fail(
                  fixable({
                    def,
                    exp,
                    literalName: getLiteralWithThis(node),
                    node,
                    prefix: ['props'],
                    selector
                  })
                );
              }
              // this.props.(stateData|xxx).a.b
              if (
                assertThisProps(node) &&
                assertThisPropsX(node) &&
                assertThisPropsXIsAnyLiteral(literalName, node)
              ) {
                return fail(
                  fixable({
                    def,
                    exp,
                    literalName: getLiteralWithThis(node),
                    node,
                    prefix: ['this', 'props'],
                    selector
                  })
                );
              }
            }
          } else {
            if (checkProps) {
              // check props.stateData.a.b && this.props.stateData.a.b cases
              // props.stateData.a.b
              if (assertPropsX(node)) {
                // return fail();
                return fail(
                  fixable({
                    def,
                    exp,
                    literalName,
                    node,
                    prefix: [`props`],
                    selector
                  })
                );
              }
              // this.props.stateData.a.b
              if (
                assertThisProps(node) &&
                assertThisPropsX(node) &&
                assertThisPropsXIsLiteral(literalName, node)
              ) {
                return fail(
                  fixable({
                    def,
                    exp,
                    literalName,
                    node,
                    prefix: [`this`, `props`],
                    selector
                  })
                );
              }
            }
            // stateData.a.b
            if (assertXIsLiteral(literalName, node)) {
              // && stateData.a.b
              // (assumes the left of the operator is stateData.a right now)
              // TODO: make this more robust?
              if (assertInAnd(node)) {
                return;
              }
              return fail(
                fixable({
                  prefix: false,
                  literalName,
                  selector,
                  def,
                  exp,
                  node
                })
              );
            }
          }
        }
      }
    };
  }
};
